$(document).ready(function () {
    // показываем label для формы, если в ней начали набирать текст @TODO: сделать правильное определение при начальное загрузки странице
    $('.form__input').on('input', function(e) {
        var input = $(e.currentTarget),
            row = input.parent('.form__row'),
            form = input.parents('.form');

        if (input.val()) {
            row.removeClass('form__row_empty');
            form.removeClass('form_empty');
        } else {
            row.addClass('form__row_empty');
            form.addClass('form_empty');
        }
    });

    $('.customers .radio__input').on('change', function(e) {
        $('.customer').removeClass('customer_active');
        $(this).parents('.customer').addClass('customer_active');
        $('.registration-form').removeClass('registration-form_hidden');
        $('.header-plain').removeClass('header-plain_full');
    });

    // открываем меню для шапки
    $('.menu-opener').on('click', function() {
        $('.menu-panel').addClass('menu-panel_open');
        $('.paranja').addClass('paranja_visible');
    })

    $('.menu-panel__close-icon').on('click', function() {
        $('.menu-panel').removeClass('menu-panel_open');
        $('.paranja').removeClass('paranja_visible');
    })

    // открываем ответ на вопрос
    $('.faq__question').on('click', function() {
        $(this).parent().toggleClass('faq__item_open');
        $(this).siblings().toggle();
    })

    // подсвечиваем кнопку подписки
    $('.subscribe__email').on('input', function(e) {
        var subscribeBtn = $('.subscribe-button');

        $(this).val() ?
            subscribeBtn
                .prop('disabled', false)
                .addClass('subscribe-button_active') :
            subscribeBtn
                .prop('disabled', true)
                .removeClass('subscribe-button_active');
    })

    // открываем меню для шапки
    $('.radio__input').on('change', function() {
        var radio = $(this).closest('.radio'),
            radioItem = $(this).parents('.radio-item'),
            radioGroup = $(this).closest('.radio-group');

        if (radioItem) {
            radioGroup.find('.radio-item_selected').removeClass('radio-item_selected');
            radioItem.addClass('radio-item_selected');
        }

        radioGroup.find('.radio_checked').removeClass('radio_checked')
        $(this).is(':checked') && radio.addClass('radio_checked');
    })

    $('.delivery .radio__input').on('change', function() {
        $('.checkout-payment').show();

        if ($(this).prop('value') === 'novaposhta') {
            $('.post-info').show();
        } else {
            $('.post-info').hide();
        }
    });

    $('.checkout-payment .radio__input').on('change', function() {
        $('.checkout-comments').show();
        $('.bill-wrap').show();
        $('.checkout-submit').show();
    });

    // инитим табы
    function removeActivetabs() {
        var activeTab = $('.tabs-menu__item_selected'),
            activeTabId = activeTab.attr('id'),
            activePane = $('[aria-labelledby="' + activeTabId + '"]');

        activeTab.removeClass('tabs-menu__item_selected');
        activePane.removeClass('tabs-panes__pane_active');
    }

    function setActiveTabs(elem) {
        var elemId =  elem.attr('id');

        elem.addClass('tabs-menu__item_selected');
        $('[aria-labelledby="' + elemId + '"]').addClass('tabs-panes__pane_active');
    }

    function scrollToActive(elem) {
        var elemPositionLeft =  elem.position().left;

        $('.tabs-menu__wrap').scrollLeft(elemPositionLeft);
    }

    $('.tabs-menu__item').on('click', function(e) {
        var item = $(e.currentTarget);

        removeActivetabs();
        setActiveTabs(item);
        scrollToActive(item)
    });

    // @TODO: добавить проверки
    $('.product-details__params').on('input', function(e) {
        var checkoutBtn = $('.checkout-btn'),
            price = parseFloat($('.product-details__price-number').text().replace(',', '.')),
            sum = 0;

        $('.product-details__input').each(function() {
            sum += Number($(this).val());
        });

        if (sum) {
            checkoutBtn.removeClass('checkout-btn_empty');
            $('.checkout-btn-vol').text(sum);
            !isNaN(price) &&  $('.checkout-btn-price').text(sum * price);
        } else {
            checkoutBtn.addClass('checkout-btn_empty');
        }
    });

    $('.catalog__summary-icon_open').on('click', function() {
        $('.catalog__summary').height('auto');
        $(this).hide();
    });

    $('.add-address__btn').on('click', function() {
        $('.add-address__box').removeClass('add-address__box_hidden');
        $(this).addClass('add-address__btn_hidden')
    });

    $('.add-address__close').on('click', function() {
        $('.add-address__box').addClass('add-address__box_hidden');
        $('.add-address__btn').removeClass('add-address__btn_hidden');
    });

	$('.header-menu__item-submenu').on('click', function() {
		$(this).toggleClass('header-menu__item-submenu_open');
	});

  $('.currency') && $('.currency').selectric({
    optionsItemBuilder: function(itemData, element, index) {
      return element.val().length ? '<span class="ico ico-' + element.val() +  '"></span>' + itemData.text : itemData.text;
    },
    labelBuilder: function(currItem) {
      return currItem.value.length ? '<span class="ico ico-' + currItem.value +  '"></span>' + currItem.text : currItem.text;
    }
  });

  $('.delivery-select').selectric();
  $('.price-select').selectric();

  $(document).on('scroll', function(e) {
    if ($('.product-details').length) {
      var scrollTop = $(document).scrollTop(),
       shouldChangePosition = $(document).height() - scrollTop - $('.footer').height() < $('.product-details').height();

      if (scrollTop < 109) {
        $('.product-details').removeClass('product-details_scroll');
      }

      if (scrollTop > 109 && !shouldChangePosition) {
        $('.product-details').addClass('product-details_scroll');
      }

      if (shouldChangePosition) {
        $('.product-details').removeClass('product-details_fixed');
      } else {
        $('.product-details').addClass('product-details_fixed');
      }

    }
  })
});
