var gulp = require('gulp');
var less = require('gulp-less');
var pug = require('gulp-pug');
var data = require('gulp-data');
var include = require('gulp-include');
var uglify = require('gulp-uglify');
var watch = require('gulp-watch');
var cleanCSS = require('gulp-clean-css');
var path = require('path');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var browserSyncObj = require('browser-sync');
var notify = require("gulp-notify");
var browserSync = require('browser-sync').create();
var reload = browserSync.reload;

var APP_PATH = './app/';
var DIST_PATH = './dist/';
var TMP_PATH = './tmp/';


gulp.task('templates', function () {
    return gulp.src(APP_PATH + 'views/*.pug')
        .pipe(pug({pretty: true}))
        .pipe(gulp.dest(DIST_PATH + 'html'));
});

gulp.task('less', function () {
    return gulp.src(APP_PATH + 'less/*.less')
        .pipe(sourcemaps.init())
        .pipe(include())
        .on('error', console.log)
        .pipe(less())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(TMP_PATH + 'css'));
});

gulp.task('css', ['less'], function () {
    return gulp.src(TMP_PATH + 'css/*.css')
        .pipe(
            autoprefixer({
                browsers: [
                    'last 4 Chrome versions',
                    'last 4 ff versions',
                    'ie >= 9',
                    'last 2 Safari versions',
                    'ios_saf >= 7',
                    'Android >= 4',
                    'last 4 and_chr versions',
                    'last 4 and_ff versions'
                ],
                remove: false
            })
        )
        .pipe(cleanCSS({compatibility: 'ie9'}))
        .pipe(gulp.dest(DIST_PATH + 'css'));
});

gulp.task('js', function () {
    return gulp.src(APP_PATH + 'js/*.js')
        .pipe(include())
        .on('error', console.log)
        .pipe(uglify())
        .pipe(gulp.dest(DIST_PATH + 'js'));
});

gulp.task('images', function () {
    return gulp.src(APP_PATH + 'images/**/*.+(jpg|gif|png|svg)')
        .pipe(gulp.dest(DIST_PATH + 'images'))
});

gulp.task('fonts', function () {
  return gulp.src(APP_PATH + 'fonts/**/*.+(eot|ttf|svg|woff|woff2)')
    .pipe(gulp.dest(DIST_PATH + 'fonts'));
});

gulp.task('imagemin', function () {
  gulp.src(APP_PATH + 'images/**/*.+(jpg|gif|png)')
    .pipe(imagemin({
      progressive: true,
      svgoPlugins: [{removeViewBox: false}],
      use: [pngquant()],
      interlaced: true
    }))
    .pipe(gulp.dest(DIST_PATH + 'images'));
});

gulp.task('dist', ['templates', 'css', 'js', 'images']);

gulp.task('default', ['dist'], function () {
    browserSync.init({
        server: {
          baseDir: DIST_PATH
        }
    });

    gulp.watch(APP_PATH + 'less/**/*.less', ['css', reload]);
    gulp.watch(APP_PATH + 'templates/**/*.pug', ['templates', reload]);
    gulp.watch(APP_PATH + 'js/**/*.js', ['js', reload]);
    gulp.watch(APP_PATH + 'images/**/*.+(jpg|gif|png)', ['images', reload]);
});
